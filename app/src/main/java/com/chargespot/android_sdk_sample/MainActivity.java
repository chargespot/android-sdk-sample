package com.chargespot.android_sdk_sample;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.chargespot.sdk.CSChargeSpotManager;
import com.chargespot.sdk.api.v1.model.ChargeSpotGroupHookListModel;
import com.chargespot.sdk.api.v1.model.ChargeSpotHookListModel;
import com.chargespot.sdk.events.CSChargeSpotEvent;
import com.chargespot.sdk.events.CSGroupEvent;
import com.chargespot.sdk.events.ICSChargeSpotEventHandler;
import com.chargespot.sdk.events.ICSGroupEventHandler;
import com.chargespot.sdk.exceptions.CSException;
import com.chargespot.sdk.manager.chargespot.ICSChargeSpotManagerInstance;
import com.chargespot.sdk.model.CSChargeSpot;
import com.chargespot.sdk.state.CSDeviceState;
import com.chargespot.sdk.state.ICSDeviceStateHandler;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements
        ICSGroupEventHandler,
        ICSChargeSpotEventHandler,
        ICSDeviceStateHandler {

    private String TAG = this.getClass().getCanonicalName();
    static final int BLUETOOTH_REQUEST_CODE = 1;
    static final int FINE_LOCATION_REQUEST_CODE = 2;
    static final int NOTIFICATION_ID = 11;

    // ChargeSpot SDK
    private ICSChargeSpotManagerInstance mChargeSpotManager;
    private CSDeviceState currentDeviceState;
    private CSChargeSpot currentChargeSpot;
    private ChargeSpotHookListModel currentHookList;

    private BluetoothAdapter mBluetoothAdapter;
    private Intent enableBtIntent;
    private NotificationManager mNotificationManager;
    private LocationManager mLocationManager;

    private boolean locationEnabled;
    private boolean mIsInForegroundMode;

    private TextView statusTextView;

    private String savedUrl;

    // Catches events when user turns off bluetooth in the app
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                        == BluetoothAdapter.STATE_OFF) {
                    MainActivity.this.mChargeSpotManager = null;
                    MainActivity.this.getPermissions();
                } else if(intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                        == BluetoothAdapter.STATE_ON) {
                    MainActivity.this.startChargeSpotManager();
                }
            }
        }
    };

    private final class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location locFromGps) {}

        @Override
        public void onProviderDisabled(String provider) {
            MainActivity.this.locationEnabled = false;
            MainActivity.this.updateText();
        }

        @Override
        public void onProviderEnabled(String provider) {
            MainActivity.this.locationEnabled = true;
            MainActivity.this.updateText();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    }

    // Required permissions
    private static final String[] REQUIRED_PERMS={
        Manifest.permission.ACCESS_FINE_LOCATION
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.startChargeSpotManager();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.registerServices();
        this.mIsInForegroundMode = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterServices();
        this.mIsInForegroundMode = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Bundle extras = intent.getExtras();
        if (extras != null && extras.containsKey("CHARGESPOT_URL")) {
            this.savedUrl = extras.getString("CHARGESPOT_URL");
            Timer intentTimer = new Timer();
            TimerTask intentTimerTask = new TimerTask() {
                @Override
                public void run() {
                    MainActivity.this.openBrowser(MainActivity.this.savedUrl);
                    MainActivity.this.savedUrl = null;
                }
            };
            intentTimer.schedule(intentTimerTask, 1000);
        }
    }

    /**
     * Initiates Activity Views
     */
    private void init() {
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        this.mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        this.mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        this.locationEnabled = this.isLocationEnabled();
        this.statusTextView = (TextView)this.findViewById(R.id.statusTextView);
        this.updateText();
    }

    /**
     * Registers broadcast receiver and instantiates services
     */
    private void registerServices() {
        registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }

    /**
     * Unregisters receivers
     */
    private void unregisterServices() {
        unregisterReceiver(mReceiver);
    }

    /**
     * Calls chargespot manager to retrieve device state
     */
    private void retrieveDeviceState() {
        try {
            this.mChargeSpotManager.retrieveDeviceState(this);
        } catch (Exception e) { throw new RuntimeException(e);}
    }

    private void updateText() {
        String statusString = this.getString(R.string.chargespot_ready);
        if (!this.locationEnabled) {
            statusString = this.getString(R.string.chargespot_needs_location);
        }
        this.statusTextView.setText(statusString);
    }

    /**
     * Callback for retrieveDeviceState()
     * Currently the variables are not used but can be included in future implementations
     * @param csDeviceState
     * @param csChargeSpot
     */
    @Override
    public void onDeviceStateRetrievedSuccess(CSDeviceState csDeviceState, CSChargeSpot csChargeSpot) {
        this.currentDeviceState = csDeviceState;
        this.currentChargeSpot = csChargeSpot;
    }

    /**
     * Success callback for group events
     * @param csGroupEvent
     * @param chargeSpotGroupHookListModel
     */
    @Override
    public void onGroupEventSuccess(CSGroupEvent csGroupEvent, ChargeSpotGroupHookListModel chargeSpotGroupHookListModel) {

    }

    /**
     * Failure callback for group events
     * @param e
     */
    @Override
    public void onGroupEventFailure(CSException e) {

    }

    /**
     * Success callback for chargespot events
     * @param csChargeSpotEvent
     * @param chargeSpotHookListModel
     */
    @Override
    public void onChargeSpotEventSuccess(CSChargeSpotEvent csChargeSpotEvent, ChargeSpotHookListModel chargeSpotHookListModel) {
        this.currentHookList = chargeSpotHookListModel;
        if (this.currentHookList != null) {
            String hookUrl = this.currentHookList.hook[0].action_url;

            if (this.mIsInForegroundMode) {
                this.openBrowser(hookUrl);
            } else {
                this.sendNotification(this.currentHookList);
            }
        }

        try {
            if (this.mChargeSpotManager != null && this.hasSufficientPermissions()) {
                this.retrieveDeviceState();
            }
        } catch (Exception e) { throw new RuntimeException(e); }
    }

    /**
     * Failure callback for chargespot events
     * @param e
     */
    @Override
    public void onChargeSpotEventFailure(CSException e) {
    }

    /**
     * Callback for requesting permissions
     * Starts ChargeSpot Manager if location permission is granted
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == FINE_LOCATION_REQUEST_CODE) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        if (this.mChargeSpotManager == null) {
                            this.startChargeSpotManager();
                        }
                    } else {
                        this.mChargeSpotManager = null;
                    }
                }
            }
        }
    }

    /**
     * Callback for startActivityWithResult
     * Starts ChargeSpot Manager on success
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == BLUETOOTH_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                this.startChargeSpotManager();
            } else {
                this.mChargeSpotManager = null;
            }
        }
    }

    /**
     * Initiates ChargeSpot Manager
     */
    private void initChargeSpotManager() {
        this.mChargeSpotManager = CSChargeSpotManager.getInstance();

        try {
            this.registerForEvents();
        } catch (Exception e) { throw new RuntimeException(e);}
    }

    /**
     * Register for ChargeSpot and Group Events
     * @throws Exception
     */
    private void registerForEvents() throws Exception {
        this.mChargeSpotManager.setupChargeSpotSDK(this, this.getString(R.string.chargespot_token));
        this.mChargeSpotManager.registerForChargeSpotEvents(this);
        this.mChargeSpotManager.registerForGroupEvents(this);
    }

    /**
     * Starts ChargeSpot Manager
     * Requests for permission if they are not sufficient
     */
    private void startChargeSpotManager() {
        if (this.hasSufficientPermissions()) {
            this.initChargeSpotManager();
        } else {
            this.getPermissions();
        }
    }

    /**
     * Builds and displays notification based on a string
     * @param notification
     */
    private void onShowNotification(String notification, String url) {
        android.support.v4.app.NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_flash)
                        .setContentTitle("ChargeSpot")
                        .setAutoCancel(true)
                        .setContentText(notification);

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.setAction(Intent.ACTION_MAIN);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        if (url != null) {
            resultIntent.putExtra("CHARGESPOT_URL", url);
        }

        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        this.mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    /**
     * Retrieve notification text and forwards it to onShowNotification
     */
    private void sendNotification(ChargeSpotHookListModel hookList) {
        String notificationText = hookList.hook[0].notification_text;
        String hookUrl = hookList.hook[0].action_url;

        if (notificationText != null && notificationText.length() > 0) {
            this.onShowNotification(notificationText, hookUrl);
        }
    }

    /**
     * Passes the url to a view action intent, usually a browser
     * @param url
     */
    private void openBrowser(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    /**
     * Check for fine location permission and bluetooth status
     * @return boolean
     */
    private boolean hasSufficientPermissions() {
        // Marshmallow and up requires additional permissions
        if (Build.VERSION.SDK_INT >= 23 && !canAccessLocation()) {
            return false;
        }

        if (!this.hasBluetooth() || !this.bluetoothEnabled()) {
            return false;
        }

        return true;
    }

    /**
     * Requests fine location permission and bluetooth
     */
    private void getPermissions() {
        // Marshmallow and up requires additional permissions
        if (Build.VERSION.SDK_INT >= 23) {
            if (!canAccessLocation()) {
                requestPermissions(REQUIRED_PERMS, FINE_LOCATION_REQUEST_CODE);
            }
        }

        if (this.hasBluetooth() && !this.bluetoothEnabled()) {
            this.enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            this.startActivityForResult(enableBtIntent, BLUETOOTH_REQUEST_CODE);
        }
    }

    /**
     * Checks if the location services is currently turned on
     * @return boolean
     */
    private boolean isLocationEnabled() {
        int locationMode;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else{
            locationProviders = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    /**
     * Checks current permission for fine location
     * @return boolean
     */
    private boolean canAccessLocation() {
        if (this.hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            LocationListener locationListener = new MyLocationListener();
            try {
                this.mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 35000, 10, locationListener);
            } catch (SecurityException se) { Log.d(TAG, "No location permission."); }
            return true;
        }
        return false;
    }

    /**
     * Check for bluetooth on device
     * @return boolean
     */
    private boolean hasBluetooth() {
        return this.mBluetoothAdapter != null;
    }

    /**
     * Check for bluetooth status
     * @return boolean
     */
    private boolean bluetoothEnabled() {
        return this.mBluetoothAdapter.isEnabled();
    }

    @TargetApi(23)
    private boolean hasPermission(String string) {
        return this.checkSelfPermission(string) == PackageManager.PERMISSION_GRANTED;
    }
}